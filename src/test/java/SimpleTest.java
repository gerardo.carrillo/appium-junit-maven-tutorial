import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import org.junit.Assert;
import org.junit.Test;

public class SimpleTest extends BaseConfig{
    KeyEvent enter = new KeyEvent(AndroidKey.ENTER);

    @Test
    public void showHomePage() throws InterruptedException {
        System.out.println("Ingresando a web Google");
        driver.get("https://www.google.com/");
        System.out.println("Insertando valor de busqueda");
        driver.findElementByAndroidUIAutomator("new UiSelector().className(\"android.widget.EditText\")").sendKeys("Cleverit Group");
        driver.findElementByAndroidUIAutomator("new UiSelector().className(\"android.widget.EditText\")").click();
        System.out.println("Realizar busqueda");
        driver.pressKey(enter);
        Thread.sleep(1000);
        System.out.println("Seleccionando resultado");
        driver.findElementByAndroidUIAutomator("new UiSelector().textMatches(\"https://cleveritgroup.com\")").click();
        Thread.sleep(1500);
        System.out.println("Validando Resultado");
        Assert.assertEquals("cleveritgroup.com",driver.findElementByAndroidUIAutomator("new UiSelector().resourceId (\"com.android.chrome:id/url_bar\")").getText());
    }
}

