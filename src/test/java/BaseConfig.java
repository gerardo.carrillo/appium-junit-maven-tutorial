import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.net.MalformedURLException;
import java.net.URL;

public class BaseConfig {

    AndroidDriver<MobileElement> driver;
    WebDriverWait wait;


    @Before
    public void setUp() throws MalformedURLException {
        DesiredCapabilities caps= new DesiredCapabilities();
        caps.setCapability(MobileCapabilityType.AUTOMATION_NAME,"UiAutomator2");
        caps.setCapability(MobileCapabilityType.PLATFORM_NAME,"ANDROID");
        caps.setCapability(MobileCapabilityType.PLATFORM_VERSION,"11");
        caps.setCapability(MobileCapabilityType.DEVICE_NAME,"Pixel");
        caps.setCapability(MobileCapabilityType.BROWSER_NAME,"Chrome");
        URL url = new URL("http://127.0.0.1:4723/wd/hub");
        driver = new AndroidDriver<>(url,caps);
        wait = new WebDriverWait(driver, 10);
        driver.context("NATIVE_APP");
        System.out.println("Iniciando....");
    }

    @After
    public void tearDown(){
        System.out.println("Finalizando..");
        driver.quit();
    }

}
